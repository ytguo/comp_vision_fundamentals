# Project 1: Convolution and Hybrid Images

# Setup
1. Install [Miniconda](https://conda.io/miniconda.html)
2. Download and extract the project code
3. Create a conda environment using the appropriate command. On Windows, open the installed "Conda prompt" to run the command. On MacOS and Linux, you can just use a terminal window to run the command, Modify the command based on your OS (`linux`, `mac`, or `win`): `conda env create -f proj1_env_<OS>.yml`
4. This will create an environment named 'cs6476_proj1'. Activate it using the Windows command, `activate cs6476_proj1` or the MacOS / Linux command, `conda activate cs6476_proj1` or `source activate cs6476_proj1`
5. Install the project package, by running `pip install -e .` inside the repo folder. This might be unnecessary for every project, but is good practice when setting up a new `conda` environment that may have `pip` requirements.
6. Run the notebook using `jupyter notebook ./proj1_code/proj1.ipynb`