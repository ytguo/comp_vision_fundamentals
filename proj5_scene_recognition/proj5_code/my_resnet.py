import torch
import torch.nn as nn

from torchvision.models import resnet18


class MyResNet18(nn.Module):
    def __init__(self):
        """
        Initializes network layers.

        Note: Do not forget to freeze the layers of ResNet except the last one.
        Note: Use 'mean' reduction in the loss_criterion. Read Pytorch's
            documention to understand what this means.

        Download pretrained ResNet using pytorch's API.

        Hint: see the import statements
        """
        super().__init__()

        self.conv_layers = None
        self.fc_layers = None
        self.loss_criterion = None

        #######################################################################
        # Student code begins
        #######################################################################
        #pretrained model
        model = resnet18(pretrained=True)
        #print("model:", model) 
        
        for param in model.parameters():
            param.requires_grad = False        
        
        self.conv_layers = nn.Sequential(*list(model.children())[:-1])

        #print("conv_layers: ", self.conv_layers)

        """
        for layer in self.conv_layers:
            if (isinstance(layer, nn.Conv2d)):
                layer.weight.requires_grad = False
                layer.bias.requires_grad = False
        """

        self.fc_layers = nn.Sequential(
            nn.Linear(512,15)
        )

        self.loss_criterion = nn.CrossEntropyLoss(reduction="mean")
        #######################################################################
        # Student code ends
        #######################################################################

    def forward(self, x: torch.Tensor) -> torch.Tensor:
        """
        Perform the forward pass with the net, duplicating grayscale channel to
        3 channels.

        Args:
            x: tensor of shape (N,C,H,W) representing input batch of images

        Returns:
            y: tensor of shape (N,num_classes) representing the output
                (raw scores) of the network. Note: we set num_classes=15
        """
        model_output = None
        x = x.repeat(1, 3, 1, 1)  # as ResNet accepts 3-channel color images
        #######################################################################
        # Student code begins
        #######################################################################
        x = self.conv_layers(x)
        x = torch.flatten(x, 1)
        x = self.fc_layers(x)
        model_output = x
        #######################################################################
        # Student code ends
        #######################################################################
        return model_output
