# CS 6476 Project 5: [Scene Recognition with Deep Learning]((https://www.cc.gatech.edu/~hays/compvision/proj5/proj5.pdf))

## Setup
1. Install [Miniconda](https://conda.io/miniconda.html). It doesn't matter whether you use Python 2 or 3 because we will create our own environment that uses 3 anyway.
2. Create a conda environment using the appropriate command. On Windows, open the installed "Conda prompt" to run the command. On MacOS and Linux, you can just use a terminal window to run the command, Modify the command based on your OS (`linux`, `mac`, or `win`): `conda env create -f proj5_env_<OS>.yml`
3. This should create an environment named 'proj5'. Activate it using the Windows command, `activate cs6476_proj5` or the MacOS / Linux command, `source activate cs6476_proj5`, `conda activate cs6476_proj5`
4. Download the data zip file via `wget https://cc.gatech.edu/~hays/compvision/proj5/data.zip` and unzip it with `unzip data.zip`
5. Install the project package, by running `pip install -e .` inside the repo folder.
6. Run the notebook using `jupyter notebook ./proj5_code/proj5.ipynb`