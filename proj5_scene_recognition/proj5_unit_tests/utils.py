import sys

def verify(function) -> str:
    """Will indicate with a print statement whether assertions passed or failed
    within function argument call.
    Args:
    - function: Python function object
    Returns:
    - string
    """
    try:
        function()
        return '\x1b[32m"Correct"\x1b[0m'
    except AssertionError as e:
        print('error message: ', e)
        print("Unexpected error:", sys.exc_info()[0])
        return '\x1b[31m"Wrong"\x1b[0m'
