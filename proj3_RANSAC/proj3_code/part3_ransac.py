import numpy as np
import math

from proj3_code.part2_fundamental_matrix import estimate_fundamental_matrix


def calculate_num_ransac_iterations(
    prob_success: float, sample_size: int, ind_prob_correct: float) -> int:
    """
    Calculates the number of RANSAC iterations needed for a given guarantee of
    success.

    Args:
        prob_success: float representing the desired guarantee of success
        sample_size: int the number of samples included in each RANSAC
            iteration
        ind_prob_success: float representing the probability that each element
            in a sample is correct

    Returns:
        num_samples: int the number of RANSAC iterations needed

    """
    num_samples = None
    ###########################################################################
    # TODO: YOUR CODE HERE                                                    #
    ###########################################################################
    print(prob_success)
    print(sample_size)
    print(ind_prob_correct)
    num_samples = np.log(1-prob_success)/np.log(1-ind_prob_correct**sample_size)

    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return int(num_samples)


def ransac_fundamental_matrix(
    matches_a: np.ndarray, matches_b: np.ndarray) -> np.ndarray:
    """
    For this section, use RANSAC to find the best fundamental matrix by
    randomly sampling interest points. You would reuse
    estimate_fundamental_matrix() from part 2 of this assignment and
    calculate_num_ransac_iterations().

    If you are trying to produce an uncluttered visualization of epipolar
    lines, you may want to return no more than 30 points for either left or
    right images.

    Tips:
        0. You will need to determine your prob_success, sample_size, and
            ind_prob_success values. What is an acceptable rate of success? How
            many points do you want to sample? What is your estimate of the
            correspondence accuracy in your dataset?
        1. A potentially useful function is numpy.random.choice for creating
            your random samples.
        2. You will also need to choose an error threshold to separate your
            inliers from your outliers. We suggest a threshold of 0.1.

    Args:
        matches_a: A numpy array of shape (N, 2) representing the coordinates
            of possibly matching points from image A
        matches_b: A numpy array of shape (N, 2) representing the coordinates
            of possibly matching points from image B
    Each row is a correspondence (e.g. row 42 of matches_a is a point that
    corresponds to row 42 of matches_b)

    Returns:
        best_F: A numpy array of shape (3, 3) representing the best fundamental
            matrix estimation
        inliers_a: A numpy array of shape (M, 2) representing the subset of
            corresponding points from image A that are inliers with respect to
            best_F
        inliers_b: A numpy array of shape (M, 2) representing the subset of
            corresponding points from image B that are inliers with respect to
            best_F
    """
    ###########################################################################
    # TODO: YOUR CODE HERE                                                    #
    ###########################################################################
    prob_success = 0.999
    sample_size = 9
    ind_prob_correct = 0.90
    entries = matches_a.shape[0]
    in_max = -1
    #reshape
    ones = np.ones((entries,1))
    matches_a2 = np.append(matches_a, ones, axis=1)
    a = np.tile(matches_a2, 3)
    matches_b2 = np.append(matches_b, ones, axis=1)
    b = matches_b2.repeat(3, axis=1)
    #construct matrix
    A = a*b

    iteration = calculate_num_ransac_iterations(prob_success,sample_size,ind_prob_correct)
    print("iterations:", iteration)
    for i in range(iteration):
        ind = np.random.choice(entries, size=sample_size)
        #print("ind:", ind)
        F = estimate_fundamental_matrix(matches_a[ind], matches_b[ind])
        #print(F)
        inlier = [np.abs(A.dot(F.flatten())) >= ind_prob_correct]
        if np.sum(inlier) >= np.sum(in_max):
            in_max = inlier
            best_F = F

    all_err = np.argsort(np.abs(A.dot(best_F.flatten())))[0:30]
    inliers_a=matches_a[all_err]
    inliers_b=matches_b[all_err]
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return best_F, inliers_a, inliers_b
