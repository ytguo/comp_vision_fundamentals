"""Fundamental matrix utilities."""

import numpy as np


def normalize_points(points: np.ndarray) -> (np.ndarray, np.ndarray):
    """
    Perform coordinate normalization through linear transformations.
    Args:
        points: A numpy array of shape (N, 2) representing the 2D points in
            the image

    Returns:
        points_normalized: A numpy array of shape (N, 2) representing the
            normalized 2D points in the image
        T: transformation matrix representing the product of the scale and
            offset matrices
    """
    ###########################################################################
    # TODO: YOUR CODE HERE                                                    #
    ###########################################################################
    #print("points_normalized: ",points_normalized)
    #print(points[0:10])
    mean = np.mean(points,axis=0)
    #construct offset matrix
    C = np.ndarray.tolist(np.diag([1, 1, 1]))
    #print(C)
    #print(mean)
    C[0][2] = -1*mean[0]
    C[1][2] = -1*mean[1]
    C = np.array(C)
    #print("--------------- C ---------------")
    #print(C)

    #construct scale matrix
    su = np.std(points.T[0:1] - mean[0])
    sv = np.std(points.T[1:2] - mean[1])
    S = np.diag([1/su, 1/sv, 1])

    #print("--------------- S ---------------")
    #print(S)
    #construct transformation matrix from scale and offset
    T =  S.dot(C)
    #print("--------------- T ---------------")
    #print(T)
    ones = np.ones((points.shape[0],1))
    points = np.column_stack((points, ones))

    points_normalized = T.dot(points.T).T

    points_normalized, c = np.split(points_normalized, [2], axis=1)
    #print("--------- points_normalized --------")
    #print(points_normalized)
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return points_normalized, T


def unnormalize_F(
    F_norm: np.ndarray, T_a: np.ndarray, T_b: np.ndarray) -> np.ndarray:
    """
    Adjusts F to account for normalized coordinates by using the transformation
    matrices.

    Args:
        F_norm: A numpy array of shape (3, 3) representing the normalized
            fundamental matrix
        T_a: Transformation matrix for image A
        T_B: Transformation matrix for image B

    Returns:
        F_orig: A numpy array of shape (3, 3) representing the original
            fundamental matrix
    """
    ###########################################################################
    # TODO: YOUR CODE HERE                                                    #
    ###########################################################################
    #T_b^(T)*F_norm*T_a
    F_orig = T_b.T.dot(F_norm).dot(T_a)

    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return F_orig


def estimate_fundamental_matrix(
    points_a: np.ndarray, points_b: np.ndarray) -> np.ndarray:
    """
    Calculates the fundamental matrix. You may use the normalize_points() and
    unnormalize_F() functions here.

    Args:
        points_a: A numpy array of shape (N, 2) representing the 2D points in
            image A
        points_b: A numpy array of shape (N, 2) representing the 2D points in
            image B

    Returns:
        F: A numpy array of shape (3, 3) representing the fundamental matrix
    """
    ###########################################################################
    # TODO: YOUR CODE HERE                                                    #
    ###########################################################################
    #call normalized points
    points_a, T_a = normalize_points(points_a)
    points_b, T_b = normalize_points(points_b)
    #add additional column of 1s to make points a and b the right dimensions
    ones = np.ones((points_a.shape[0],1))
    points_a = np.append(points_a, ones, axis=1)
    points_a = np.tile(points_a, 3)
    #print(points_a)
    points_b = np.append(points_b, ones, axis=1)
    points_b = points_b.repeat(3, axis=1)

    #similar to part 1 call SVD, F_norm is the last row of v1
    u1, s1, v1 = np.linalg.svd(points_a*points_b)
    F_norm = v1[-1]
    F_norm = F_norm.reshape((3, 3))
    #print(F_norm)

    #update sigma matrix
    u2, s2, v2 = np.linalg.svd(F_norm)
    s2[-1] = 0
    s2 = np.diag(s2)

    #reconstruct F matrix with updated sigma matrix
    F = u2.dot(s2).dot(v2)

    #unnormalize F
    F = unnormalize_F(F, T_a, T_b)   
    ###########################################################################
    #                             END OF YOUR CODE                            #
    ###########################################################################

    return F
